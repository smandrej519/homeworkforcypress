/// <reference types="cypress" />
describe('example to-do app', () => {
    it('before_test_negative', () => {    
      cy.visit('https://the-internet.herokuapp.com/login')
      cy.get('input#username').type('admin')
      cy.get('input#password').type('admin')
      cy.get('button').click()
      cy.contains('#flash', 'You logged into a secure area!').should('be.visible')
    })
    it('before_test_positive', () => {    
        cy.visit('https://the-internet.herokuapp.com/login')
        cy.get('input#username').type('tomsmith')
        cy.get('input#password').type('SuperSecretPassword!')
        cy.get('button').click()
        cy.contains('#flash', 'You logged into a secure area!').should('be.visible')
      })
})    